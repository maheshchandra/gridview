﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DataGridViewExample.aspx.cs" Inherits="DataGridView.DataGridViewExample" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
                 </asp:ScriptManager>
    
       
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                 <asp:GridView ID="grvStudent" runat="server"
                        ShowFooter="True" AutoGenerateColumns="False"
                        CellPadding="4" ForeColor="#333333"
                        GridLines="None" OnRowDeleting="grvStudent_RowDeleting1" OnSelectedIndexChanged="grvStudent_SelectedIndexChanged" >
                        <Columns>
                            <asp:BoundField DataField="ChapterNumber" HeaderText="SNo" />
                            <asp:TemplateField HeaderText="Chapter Title">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtChapterTitle" runat="server"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Content">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtContent" runat="server"></asp:TextBox>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right" />
                                <FooterTemplate>
                                    <asp:Button ID="ButtonAdd" runat="server"
                                        Text="Add New Row" OnClick="ButtonAdd_Click" />
                                </FooterTemplate>
                            </asp:TemplateField>                            
                            <asp:CommandField ShowDeleteButton="True" />
                        </Columns>
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <RowStyle BackColor="#EFF3FB" />
                        <EditRowStyle BackColor="#2461BF" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <AlternatingRowStyle BackColor="White" />
                    </asp:GridView>
    

                 
    

            </ContentTemplate>
        </asp:UpdatePanel>
        <br />
        <asp:Label ID="labal" runat="server" Text="Mahesh"></asp:Label>
        <asp:Button ID="btn" runat="server" OnClick="btn_Click" Text="Check" />
    
        <br />
    
    </div>
    </form>
</body>
</html>
