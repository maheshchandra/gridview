﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Xml;
using System.IO;
using System.Xml.Serialization;

namespace DataGridView
{
    public partial class DataGridViewExample : System.Web.UI.Page
    {
        #region Region for adding chapters and contents  of book seperately:

        private const string CONTROL_TXT_CHAPTER = "txtChapterTitle";
        private const string CONTROL_TXT_CONTENT = "txtContent";
        private const string TABLE_NAME = "CurrentTable";
        private const string COL_CHAPTER_TITLE = "ChapterTitle";
        private const string COl_CONTENT = "Content";
        private const string CHAPTER_NUMBER = "ChapterNumber";

        private void FirstGridViewRow()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn(CHAPTER_NUMBER, typeof(string)));
            dt.Columns.Add(new DataColumn(COL_CHAPTER_TITLE, typeof(string)));
            dt.Columns.Add(new DataColumn(COl_CONTENT, typeof(string)));
            dr = dt.NewRow();
            dr[CHAPTER_NUMBER] = 1;
            dr[COL_CHAPTER_TITLE] = string.Empty;
            dr[COl_CONTENT] = string.Empty;
            dt.Rows.Add(dr);
            ViewState[TABLE_NAME] = dt;
            grvStudent.DataSource = dt;
            grvStudent.DataBind();
        }
        private void AddNewRow()
        {
            int rowIndex = 0;

            if (ViewState[TABLE_NAME] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState[TABLE_NAME];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        TextBox TextBoxName =
                          (TextBox)grvStudent.Rows[rowIndex].Cells[1].FindControl(CONTROL_TXT_CHAPTER);
                        TextBox TextBoxAge =
                          (TextBox)grvStudent.Rows[rowIndex].Cells[2].FindControl(CONTROL_TXT_CONTENT);
                        drCurrentRow = dtCurrentTable.NewRow();
                        drCurrentRow[CHAPTER_NUMBER] = i + 1;
                        dtCurrentTable.Rows[i - 1][COL_CHAPTER_TITLE] = TextBoxName.Text;
                        dtCurrentTable.Rows[i - 1][COl_CONTENT] = TextBoxAge.Text;
                        rowIndex++;
                    }
                    dtCurrentTable.Rows.Add(drCurrentRow);
                    grvStudent.DataSource = dtCurrentTable;
                    grvStudent.DataBind();
                    ViewState[TABLE_NAME] = dtCurrentTable;
                }
            }
            else
            {
                Response.Write("ViewState is null");
            }
            SetPreviousData();
        }
        private void SetPreviousData()
        {
            int rowIndex = 0;
            if (ViewState[TABLE_NAME] != null)
            {
                DataTable dt = (DataTable)ViewState[TABLE_NAME];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        TextBox TextBoxName = (TextBox)grvStudent.Rows[rowIndex].Cells[1].FindControl(CONTROL_TXT_CHAPTER);
                        TextBox TextBoxAge = (TextBox)grvStudent.Rows[rowIndex].Cells[2].FindControl(CONTROL_TXT_CONTENT);
                        TextBoxName.Text = dt.Rows[i][COL_CHAPTER_TITLE].ToString();
                        TextBoxAge.Text = dt.Rows[i][COl_CONTENT].ToString();
                        rowIndex++;
                    }
                }
            }
        }
        private void SetRowData()
        {
            int rowIndex = 0;

            if (ViewState[TABLE_NAME] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState[TABLE_NAME];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        TextBox TextBoxName = (TextBox)grvStudent.Rows[rowIndex].Cells[1].FindControl(CONTROL_TXT_CHAPTER);
                        TextBox TextBoxAge = (TextBox)grvStudent.Rows[rowIndex].Cells[2].FindControl(CONTROL_TXT_CONTENT);
                        drCurrentRow = dtCurrentTable.NewRow();
                        drCurrentRow[CHAPTER_NUMBER] = i + 1;
                        dtCurrentTable.Rows[i - 1][COL_CHAPTER_TITLE] = TextBoxName.Text;
                        dtCurrentTable.Rows[i - 1][COl_CONTENT] = TextBoxAge.Text;
                        rowIndex++;
                    }

                    ViewState[TABLE_NAME] = dtCurrentTable;
                    //grvStudent.DataSource = dtCurrentTable;
                    //grvStudent.DataBind();
                }
            }
            else
            {
                Response.Write("ViewState is null");
            }
            //SetPreviousData();
        }

        protected void ButtonAdd_Click(object sender, EventArgs e)
        {
            AddNewRow();
        }

        protected void grvStudent_RowDeleting1(object sender, GridViewDeleteEventArgs e)
        {
            SetRowData();
            if (ViewState[TABLE_NAME] != null)
            {
                DataTable dt = (DataTable)ViewState[TABLE_NAME];
                DataRow drCurrentRow = null;
                int rowIndex = Convert.ToInt32(e.RowIndex);
                if (dt.Rows.Count > 1)
                {
                    dt.Rows.Remove(dt.Rows[rowIndex]);
                    drCurrentRow = dt.NewRow();
                    ViewState[TABLE_NAME] = dt;
                    grvStudent.DataSource = dt;
                    grvStudent.DataBind();

                    for (int i = 0; i < grvStudent.Rows.Count - 1; i++)
                    {
                        grvStudent.Rows[i].Cells[0].Text = Convert.ToString(i + 1);
                    }
                    SetPreviousData();
                }
            }
        }

        protected void btnGenerateBookXml_Click(object sender, EventArgs e)
        {
            DataTable dtBookContent = (DataTable)ViewState[TABLE_NAME];
            DataSet ds = new DataSet();
            ds.Tables.Add(dtBookContent);

            XmlElement xE = (XmlElement)Serialize(ds);
            string strXml = xE.OuterXml.ToString();

            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(strXml);
            string rndBookName = new Random().Next(100000, 10000000).ToString();
            xDoc.Save(Server.MapPath("Book_XML\\" + rndBookName + ".xml"));
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('XML Generated Successfully with name " + rndBookName + ".xml')", true);
        }

        /// <summary>
        /// Serialize given object into XmlElement.
        /// </summary>
        /// <param name="transformObject">Input object for serialization.</param>
        /// <returns>Returns serialized XmlElement.</returns>
        public XmlElement Serialize(object transformObject)
        {
            XmlElement serializedElement = null;
            try
            {
                MemoryStream memStream = new MemoryStream();
                XmlSerializer serializer = new XmlSerializer(transformObject.GetType());
                serializer.Serialize(memStream, transformObject);
                memStream.Position = 0;
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(memStream);
                serializedElement = xmlDoc.DocumentElement;
            }
            catch (Exception SerializeException)
            {

            }
            return serializedElement;
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FirstGridViewRow();
            }
        }
      

        protected void grvStudent_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btn_Click(object sender, EventArgs e)
        {
            DataTable dtBookContent = (DataTable)ViewState[TABLE_NAME];
            DataSet ds = new DataSet();
            ds.Tables.Add(dtBookContent);

            XmlElement xE = (XmlElement)Serialize(ds);
            string strXml = xE.OuterXml.ToString();

            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(strXml);
            string rndBookName = new Random().Next(100000, 10000000).ToString();
            xDoc.Save(Server.MapPath("Book_XML\\" + rndBookName + ".xml"));
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('XML Generated Successfully with name " + rndBookName + ".xml')", true);
            Response.Write("Succes");
        }
    }
}